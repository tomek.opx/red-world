using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLevel : MonoBehaviour
{
    //    public int statPoints = 10, playerLevel = 0, currentExp = 0, expToNextLvl = 1000;
    public int statPoints = 10;
    public int playerLevel = 0;
    public int currentExp = 0;
    public int expToNextLvl = 1000;
    public float expMultiplier = 1.3f;

    public void AddExp(int exp)
    {
        currentExp += exp;
        if (currentExp >= expToNextLvl)
        {
            currentExp -= expToNextLvl;
            expToNextLvl *= 2; // tu będzie wyliczało ciąg fibonacciego
            LevelUp();
        }
        Debug.Log("dodaje " + exp + " expa. Posiadasz " + currentExp + " expa.");
        Debug.Log("exp do nastepnego poziomu wynosi: " + expToNextLvl);

    }
    public void LevelUp() 
    {
        playerLevel++;
        statPoints++;
        Debug.Log("Zyskales poziom. Twoj level to :"+ playerLevel);

    }

}
