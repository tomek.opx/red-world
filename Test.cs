using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Test : PlayerLevel
{
    int int1 = 500, int2 = 1000;
   
    public void AddExp500()
    {
        AddExp(int1);

    }
    public void AddExp1000()
    {
       AddExp(int2);

    }
    public void UpLevel()
    {
        LevelUp();
    }
}
