﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RedWorld.CharacterStats;



public class PlayerStatsUI : PlayerLevel
{
    public Text LevelText, StrenghText, ConditionText, AgilityText, InteligenceText, CharismaText, WillpowerText, StatPointsText, CurrentExpText, ExpToNextLevelText;
    public GameObject character;
    private void Start()
    {
        Character c = character.GetComponent<Character>();

    }
    void Update()
    {
        StrenghText.text = c.Strength.BaseValue.ToString();
        ConditionText.text = c.Condition.BaseValue.ToString();
        AgilityText.text = c.Agility.BaseValue.ToString();
        InteligenceText.text = c.Intelligence.BaseValue.ToString();
        CharismaText.text = c.Charisma.BaseValue.ToString();
        WillpowerText.text = c.Willpower.BaseValue.ToString();

        LevelText.text = playerLevel.ToString();
        StatPointsText.text = statPoints.ToString();
        CurrentExpText.text = currentExp.ToString();
        ExpToNextLevelText.text = CurrentExpToNextLvl.ToString();
    }
    public void AddStr()
    {
        if(statPoints > 0)
        {
            Character c = FindObjectOfType<Character>();
            c.Strength.BaseValue += 1;
            statPoints--;
        }
        else
        {
            Debug.Log("Nie masz punktow statystyk");
        }
    }
}
